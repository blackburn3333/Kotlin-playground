package com.example.jay.kotlinplayground

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import org.w3c.dom.Text

class cal : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cal)

        val add = findViewById<Button>(R.id.add);
        val sub = findViewById<Button>(R.id.sub);
        val multi = findViewById<Button>(R.id.multi);
        val divid = findViewById<Button>(R.id.divi);

        val go_back = findViewById<Button>(R.id.back);

        val num_one = findViewById<EditText>(R.id.FirstNum);
        val num_two = findViewById<EditText>(R.id.SecondNum);

        val error_text = findViewById<TextView>(R.id.ErrorText);

        add.setOnClickListener {
                error_text.setText(cal_numbers(num_one.text.toString(), num_two.text.toString(), "+"));
        }

        sub.setOnClickListener {
                error_text.setText(cal_numbers(num_one.text.toString(), num_two.text.toString(), "-"));
        }

        multi.setOnClickListener {
                error_text.setText(cal_numbers(num_one.text.toString(), num_two.text.toString(), "*"));
        }

        divid.setOnClickListener {
                error_text.setText(cal_numbers(num_one.text.toString(), num_two.text.toString(), "/"));

        }

        go_back.setOnClickListener{
            startActivity(Intent(this,MainActivity
            ::class.java))
        }

    }

    fun cal_numbers(numOne: String, numTwo: String, opeRation: String): String {
        if (opeRation.equals("+")) {
            return (numOne.toInt() + numTwo.toInt()).toString();
        } else if (opeRation.equals("-")) {
            return (numOne.toInt() - numTwo.toInt()).toString();
        } else if (opeRation.equals("*")) {
            return (numOne.toInt() * numTwo.toInt()).toString();
        } else if (opeRation.equals("/")) {
            return (numOne.toInt() / numTwo.toInt()).toString();
        }
        return "Error";
    }
}
