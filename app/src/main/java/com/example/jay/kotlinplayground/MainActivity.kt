package com.example.jay.kotlinplayground

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Button;
import android.widget.EditText
import android.widget.Toast;
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val hellow_btn = findViewById<Button>(R.id.alertView);

        val name_text = findViewById<EditText>(R.id.enter_text);
        val goto_cal = findViewById<Button>(R.id.goto_cal);


        val pass_data_btn = findViewById<Button>(R.id.PassToAct);
        val pass_text = findViewById<EditText>(R.id.PassValue);

        hellow_btn.setOnClickListener{
            showToast(name_text.text.toString())
        }

        goto_cal.setOnClickListener{
            goto_activity("Cal_Act")
        }

        pass_data_btn.setOnClickListener {
            if(pass_text.text.equals("") || pass_text.text.isBlank()){
                Toast.makeText(
                        this, "Enter Someting before pass",Toast.LENGTH_LONG
                ).show();
            }else{
                val passData = pass_text.text.toString();
                val intent = Intent(this,datapass::class.java);
                intent.putExtra("PassVal",passData);
                startActivity(intent);
            }
        }
    }

    fun showToast(toastMessage: String){
        if(toastMessage.equals("")||toastMessage.isBlank()){
            Toast.makeText(
                    this, "Hellow Enter Your Name",Toast.LENGTH_LONG
            ).show();
        }else{
            Toast.makeText(
                    this, "Hellow "+toastMessage,Toast.LENGTH_LONG
            ).show();
        }
    }

    fun goto_activity(ActivityName: String){
        if(ActivityName.equals("Cal_Act")){
            startActivity(Intent(this,cal::class.java))
        }else if(ActivityName.equals("Pass_Data")){

        }
    }
}
