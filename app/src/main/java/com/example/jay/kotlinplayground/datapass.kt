package com.example.jay.kotlinplayground

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class datapass : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_datapass)

        var intent = intent
        val passData = intent.getStringExtra("PassVal");
        val viewDataText = findViewById<TextView>(R.id.viewPass);
        viewDataText.text = passData;

        val go_back = findViewById<Button>(R.id.backBtn);

        go_back.setOnClickListener{
            startActivity(Intent(this,MainActivity
            ::class.java))
        }
    }
}
